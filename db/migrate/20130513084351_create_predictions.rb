class CreatePredictions < ActiveRecord::Migration
  def change
    create_table :predictions do |t|
      t.string :home_team
      t.string :away_team
      t.integer :home_score
      t.integer :away_score
      t.integer :fixture_id
      t.integer :score
      t.date :fixture_date
      t.integer :user_id

      t.timestamps
    end
  end
end
