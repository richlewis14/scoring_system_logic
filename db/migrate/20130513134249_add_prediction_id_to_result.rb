class AddPredictionIdToResult < ActiveRecord::Migration
  def change
  	add_column :results, :prediction_id, :integer
  end

  def down
  	remove_column :results, :prediction_id

  end
end
