class CreateFixtures < ActiveRecord::Migration
  def change
    create_table :fixtures do |t|
      t.string :home_team
      t.string :away_team
      t.text :kickoff_time
      t.integer :prediction_id
      t.date :fixture_date

      t.timestamps
    end
  end
end
