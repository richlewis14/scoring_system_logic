class PredictionsController < ApplicationController
 

  def index
  	 previous_three_days = Date.today - 3
    @predictions = current_user.predictions.where("fixture_date >= ?", previous_three_days)
    @user_predictions = @predictions.group_by {|p| p.fixture_date}

  	

  end

	def new
     @prediction = Prediction.new
     three_days_from_today = Date.today - 3
     @fixtures = Fixture.where("fixture_date >= ?", three_days_from_today)
    @fixture_date = @fixtures.group_by {|fd| fd.fixture_date}
	end

	def create
    begin
      params[:predictions].each do |prediction|
        Prediction.new(prediction).save!
      end
      redirect_to root_path, :notice => 'Predictions Submitted Successfully'
    
    end
  end


end