class MemberController < ApplicationController
	before_filter :authenticate_user!
  def index
  	three_days_from_today = Date.today - 3
    @fixtures = Fixture.where("fixture_date <= ?", three_days_from_today)
    @fixture_date = @fixtures.group_by {|fd| fd.fixture_date}

    previous_three_days = Date.today - 3
    @results = Result.where("fixture_date <= ?", previous_three_days).group("home_team, away_team, fixture_date")
    @result_date = @results.group_by {|r| r.fixture_date}

    @predictions = current_user.predictions.where("fixture_date <= ?", previous_three_days)
    @user_predictions = @predictions.group_by {|p| p.fixture_date}

    @weekly_points = current_user.predictions.where("fixture_date <= ?", previous_three_days).sum(:score)
  end
end
