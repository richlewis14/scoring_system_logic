class Result < ActiveRecord::Base
  attr_accessible :away_score, :away_team, :fixture_date, :home_score, :home_team, :prediction_id
end
