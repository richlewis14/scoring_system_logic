class Prediction < ActiveRecord::Base
  attr_accessible :away_score, :away_team, :fixture_id, :home_score, :home_team, :score, :fixture_date, :user_id

  has_one :fixture
  has_one :result
 
end
